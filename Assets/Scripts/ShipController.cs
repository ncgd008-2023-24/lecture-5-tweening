using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ShipInputHandler))]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
[ExecuteAfter(typeof(ShipInputHandler))]
public class ShipController : MonoBehaviour
{
    public float FireDelay = 0.5f;
    public float Acceleration = 5f;
    public float MaxLinearVelocity = 5f;
    public float BulletInitialForceMultyplier = 10f;

    private Rigidbody _rigidbody;
    private ShipInputHandler _shipInputHandler;

    [SerializeField] private GameObject _bulletPrefab;
    [SerializeField] private GameObject _bulletRoot;


    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _shipInputHandler = GetComponent<ShipInputHandler>();
    }

    void Start()
    {
        StartCoroutine(FireCycle());
    }

    private void FixedUpdate()
    {
        _rigidbody.AddForce(_shipInputHandler.Move * Acceleration);
        _rigidbody.maxLinearVelocity = MaxLinearVelocity;
    }

    IEnumerator FireCycle()
    {
        while(true)
        {
            if(_shipInputHandler.Fire)
            {
                InitializeBullet();
            }

            yield return new WaitForSeconds(FireDelay);
        }
    }

    void InitializeBullet()
    {
        var pos = new Vector2(this.transform.position.x, this.transform.position.y);
        var dir = (_shipInputHandler.Pointer2 - pos).normalized;

        var bullet = Instantiate(_bulletPrefab);
        bullet.transform.SetParent(_bulletRoot.transform);
        bullet.transform.position = this.transform.position;
        var bulletRigid = bullet.GetComponent<Rigidbody>();
        bulletRigid.AddForce(BulletInitialForceMultyplier * dir);

        Destroy(bullet, 2f);
    }

}
