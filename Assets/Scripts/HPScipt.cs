using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HPScipt : MonoBehaviour
{
    public int HP;

    public UnityEvent<string> HPChanged;

    // Start is called before the first frame update
    void Start()
    {
        this.DOHarm(50, 5f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

/// <summary>
/// Extension methods for the <see cref="HPScipt"/> class, providing custom DoTween functionality.
/// </summary>
public static class HPSciptExtensions
{
    /// <summary>
    /// Creates a Tween for changing the HP (Health Points) of the <paramref name="script"/> over time.
    /// </summary>
    /// <param name="script">The <see cref="HPScipt"/> instance to apply the Tween to.</param>
    /// <param name="targetValue">The target HP value to tween to.</param>
    /// <param name="time">The duration of the Tween in seconds.</param>
    /// <returns>
    /// A Tween object representing the interpolation of the HP property over time.
    /// </returns>
    /// <remarks>
    /// This method uses the DOTween library to create a Tween for smoothly transitioning
    /// the HP property of the provided <paramref name="script"/> to the specified <paramref name="targetValue"/>.
    /// The <paramref name="time"/> parameter determines the duration of the Tween.
    /// </remarks>
    public static DG.Tweening.Core.TweenerCore<int, int, DG.Tweening.Plugins.Options.NoOptions>
        DOHarm(this HPScipt script, int targetValue, float time)
    {
        return DOTween.To(
            () => script.HP,
            (newValue) => {
                script.HP = newValue;
                script.HPChanged.Invoke($"{newValue} HP");
            },
            targetValue,
            time
        ).SetTarget(script);
    }
}

