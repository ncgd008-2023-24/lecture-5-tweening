using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AsteroidSpawner : Singleton<AsteroidSpawner>
{
    public List<GameObject> Prefabs;

    public Vector3 MaxPosition;
    public Vector3 MinPosition;

    public int GroupCount;
    public Vector2 GroupSize;

    public Vector2 TimeDalay;

    public UnityEvent AsteroidSpawned;
    public UnityEvent AsteroidDestroyed;

    public IEnumerator Start()
    {
        for (int i = 0; i < GroupCount; i++)
        {
            yield return new WaitForSeconds(Random.Range(TimeDalay.x, TimeDalay.y));

            var group = Random.Range(GroupSize.x, GroupSize.y);
            for (int j = 0; j < group; j++)
            {
                SpawnAsteroid();
            }
        }
    }

    private void SpawnAsteroid()
    {
        var prefab = Prefabs[Random.Range(0, Prefabs.Count)];
        var instance = Instantiate(prefab);
        instance.transform.SetParent(transform, false);
        instance.transform.localPosition = new Vector3(
            Random.Range(MinPosition.x, MaxPosition.x),
            Random.Range(MinPosition.y, MaxPosition.y),
            0
        );
        AsteroidSpawned.Invoke();
        var force = new Vector3(
            Random.Range(0f, 1f),
            Random.Range(0f, 1f),
            Random.Range(0f, 1f)
        ).normalized;
        var rigid = instance.GetComponent<Rigidbody>();
        rigid.AddForce(force * Random.Range(0, 20));
    }
}
