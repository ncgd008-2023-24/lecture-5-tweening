using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

[RequireComponent(typeof(Camera))]
public class AsteraxCameraManager : Singleton<AsteraxCameraManager>
{

    #region [SerializeField] public Camera MainCamera { get; }

    [SerializeField]
    [Tooltip("Main camera of the game")]
    [FormerlySerializedAs("qtim_MainCamera")]
    private Camera _MainCamera;

    /// <summary>
    /// Main camera of the game
    /// </summary>
    public Camera MainCamera => _MainCamera;

    #endregion

    protected override void Awake()
    {
        base.Awake();
        _MainCamera = GetComponent<Camera>();
    }

}
