using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidScript : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.GetComponent<BulletScript>())
        {
            Destroy(this.gameObject);
            Destroy(collision.gameObject);
            AsteroidSpawner.Instance.AsteroidDestroyed.Invoke();
        }
    }
}
