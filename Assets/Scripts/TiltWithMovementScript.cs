using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class TiltWithMovementScript : MonoBehaviour
{
    private Rigidbody _rigidbody;

    // Start is called before the first frame update
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        var direction = _rigidbody.velocity.normalized / 10f;
        direction = new Vector3(direction.x, direction.y, 1f);
        direction = direction.normalized;

        this.transform.rotation = Quaternion.LookRotation(Vector3.right, direction);
    }
}
