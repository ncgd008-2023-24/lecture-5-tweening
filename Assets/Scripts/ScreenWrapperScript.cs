using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class ScreenWrapperScript : MonoBehaviour
{
    public float viewPortOffset = 0.01f;

    void FixedUpdate()
    {
        var viewPort = AsteraxCameraManager.Instance.MainCamera.WorldToViewportPoint(this.transform.position);
        var newViewPort = viewPort;

        if (viewPort.x > 1 + viewPortOffset * 2f)
            newViewPort.x = -viewPortOffset;

        if (viewPort.y > 1 + viewPortOffset * 2f)
            newViewPort.y = -viewPortOffset;

        if (viewPort.x < -viewPortOffset * 2f)
            newViewPort.x = 1 + viewPortOffset;

        if (viewPort.y < -viewPortOffset * 2f)
            newViewPort.y = 1 + viewPortOffset;

        if (viewPort != newViewPort)
            this.transform.position = AsteraxCameraManager.Instance.MainCamera.ViewportToWorldPoint(newViewPort);
    }
}
