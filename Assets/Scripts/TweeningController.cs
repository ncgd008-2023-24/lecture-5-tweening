using DG.Tweening;
using MyBox;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

public class TweeningController : MonoBehaviour
{
    public float size = 5f;
    [Range(0f, 10f)]
    public float time = 2f;

    #region [SerializeField] public int CoinCount {get; set;}

    [SerializeField]
    private int _CoinCount;

    public AnimationCurve animationCurve;

    /// <summary>
    /// TODO
    /// </summary>
    public int CoinCount
    {
        get
        {
            return _CoinCount;
        }
        set
        {
            _CoinCount = value;
            InvokeOnCoinCountChanged();
        }
    }

    [SerializeField]
    private UnityEvent<string> CoinCountChanged;

    private void InvokeOnCoinCountChanged()
    {
        if (CoinCountChanged != null)
            CoinCountChanged.Invoke($"{_CoinCount}$");
    }

    #endregion

    private Sequence sequence;
    private Tween tween;
    private IEnumerator coroutine;

    [ButtonMethod()]
    public void @Reset()
    {
        if (tween != null)
            tween.Kill();
        if (sequence != null)
            sequence.Kill();
        if(coroutine != null)
            StopCoroutine(coroutine);
        _CoinCount = 0;

        transform.localPosition = new Vector3(-size, 0, 0);
        GetComponent<SpriteRenderer>().color = Color.white;
    }

    [ButtonMethod]
    public void Prototype()
    {
        transform.localPosition = new Vector3(size, 0, 0);
    }

    [ButtonMethod]
    public void MyMove()
    {
        coroutine = _update();
        StartCoroutine(coroutine);

        IEnumerator _update()
        {
            float deltaPerSec = (size * 2) / time;
            while (transform.localPosition.x < size)
            {
                float delta = deltaPerSec * Time.deltaTime;
                transform.localPosition = new Vector3(transform.localPosition.x + delta, 0, 0);
                yield return null;
            }
        }
    }

    [ButtonMethod]
    public void Lerp()
    {
        coroutine = _update();
        StartCoroutine(coroutine);

        IEnumerator _update()
        {
            float progTime = 0;

            while (progTime < time)
            {
                progTime += Time.deltaTime;
                var lerp = Mathf.Lerp(-size, size, progTime / time);
                transform.localPosition = new Vector3(lerp, 0, 0);
                yield return null;
            }
        }
    }

    [ButtonMethod]
    public void BasicTweenLinear()
    {
        tween = transform.DOLocalMove(new Vector3(size, 0, 0), time).SetEase(Ease.InOutBounce);
    }

    [ButtonMethod]
    public void BasicTween()
    {
        tween = transform.DOLocalMove(new Vector3(size, 0, 0), time);
    }

    [ButtonMethod]
    public void BasicTween2()
    {
        tween = transform.DOLocalMoveX(size, time);
    }

    [ButtonMethod]
    public void TweenBounce()
    {
        tween = transform.DOLocalMoveX(size, time)
            .SetEase(Ease.OutBounce);
    }

    [ButtonMethod]
    public void BasicTweenSequence()
    {
        sequence = DOTween.Sequence()
            .Append(transform.DOMoveX(size, time))
            .Append(transform.DOMoveX(-size, time));
    }

    [ButtonMethod]
    public void Loops()
    {
        sequence = DOTween.Sequence()
            .Append(transform.DOMoveX(size, time))
            .Append(transform.DOMoveX(-size, time))
            .SetLoops(3);
    }

    [ButtonMethod]
    public void InifniteLoops()
    {
        sequence = DOTween.Sequence()
            .Append(transform.DOMoveX(size, time))
            .Append(transform.DOMoveX(-size, time))
            .SetLoops(-1);
    }

    [ButtonMethod]
    public void TweenSequenceJoin()
    {
        var renderer = GetComponent<SpriteRenderer>();
        sequence = DOTween.Sequence()
            .Append(transform.DOMoveX(size, time))
            .Join(renderer.DOColor(Color.red, time));
    }

    [ButtonMethod]
    public void TweenSequenceInsert()
    {
        var renderer = GetComponent<SpriteRenderer>();
        sequence = DOTween.Sequence()
            .Insert(1, transform.DOLocalMoveX(size, time))
            .Insert( 1 + time / 2f, renderer.DOColor(Color.red, time / 2));
    }

    [ButtonMethod]
    public void TweenSequenceFor()
    {
        sequence = DOTween.Sequence();
        for (int i = 0; i <= 10; i++)
        {
            sequence.Append(transform.DOLocalMoveX(-size + i * (size / 5), time));
            sequence.AppendInterval(0.25f);
        }

    }

    [ButtonMethod]
    public void DOBlendableMoveBy()
    {
        transform.DOBlendableMoveBy(new Vector3(2 * size, 0, 0), time);
        transform.DOBlendableMoveBy(new Vector3(0, 2, 0), time);
    }

    [ButtonMethod]
    public void DOCustom()
    {
        DOTween.To(
            () => CoinCount,
            x => CoinCount = x,
            100,
            time
        );
    }

    [ButtonMethod]
    public void CompleteAll()
    {
        Debug.Log(DOTween.TweensByTarget(transform));
        DOTween.CompleteAll(false);
    }

}
