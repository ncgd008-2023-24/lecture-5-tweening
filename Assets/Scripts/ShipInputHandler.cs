using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class ShipInputHandler : Singleton<ShipInputHandler>
{
    [field: SerializeField]
    public Vector3 Move { get; private set; }


    [field: SerializeField]
    public Vector2 Pointer2 { get; private set; }
    public Vector3 Pointer3 => new Vector3(Pointer2.x, Pointer2.y, 0);


    [field: SerializeField]
    public bool Fire { get; private set; }


    public void OnMove(InputAction.CallbackContext context)
    {
        Move = context.ReadValue<Vector3>();
    }

    public void OnPointer(InputAction.CallbackContext context)
    {
        Pointer2 = context.ReadValue<Vector2>();
        Pointer2 = AsteraxCameraManager.Instance.MainCamera.ScreenToWorldPoint(Pointer2);
    }

    public void OnFire(InputAction.CallbackContext context)
    {
        Fire = context.ReadValueAsButton();
    }
}
